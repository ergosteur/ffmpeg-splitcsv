#!/usr/bin/env python
# encoding: utf-8
"""
timestampParser.py
 
Matthieu Yiptong - 2019-04-26.
"""
import wx
import csv
import subprocess
import os
import os.path
import sys
from datetime import timedelta, datetime
import shutil

# Global vars
csv_file = None
media_file = None
output_dir = None
ffmpeg_path = shutil.which("ffmpeg")

def do_encode(inputFile, inputCsv, encType, outputDir):
    #inputFile = "ASMR _ WHEN WE ALL FALL ASLEEP, WHERE DO WE GO _ Full Album Billie Eilish Tribute-ZsZA_19xIRI.mp4"
    #inputFile = sys.argv[1]
    #inputCsv = sys.argv[2]
    #encType = sys.argv[3]
    if encType == "audioonlycopy":
        encVideo = "remove"
        encAudio = "copy"
    elif encType == "enc":
        encVideo = "h264"
        encAudio = "aac"
    elif encType == "audioonlyenc":
        encVideo = "remove"
        encAudio = "aac"
    else: #encType == "copy":
        encVideo = "copy"
        encAudio = "copy"
    def getVideoEncodeFlag():
        if encVideo == "remove":
            return "-vn"
        else:
            return "-c:v"
    def printTrackInfo(start, title):
        print("Starting time: " + start)
        print("Segment Title: " + title)
    def ffmpegSplit(sourceFile, start, duration, title, outputDir, outputFile, trackNumStr):
        output_full_path = os.path.join(outputDir, trackNumStr + " " + outputFile + ".mp4")
        if duration is None:
            args = [ffmpeg_path, "-y", "-ss", start, "-i", sourceFile, getVideoEncodeFlag(), encVideo, "-c:a", encAudio, "-metadata", "title=" + title, "-metadata", "track=" + trackNumStr, output_full_path]
        else:
            args = [ffmpeg_path, "-y", "-ss", start, "-i", sourceFile, "-t", duration, getVideoEncodeFlag(), encVideo, "-c:a", encAudio, "-metadata", "title=" + title, "-metadata", "track=" + trackNumStr, output_full_path]
        if encVideo == "remove":
            args.remove("remove")
        print(args)
        FNULL = open(os.devnull, 'w')
        subprocess.call(args) #, stdout=FNULL, stderr=FNULL, shell=False)
    def getTrackTime(timeStr1, timeStr2):
        #timeStr1 = "00:10"
        #timeStr2 = "03:01"
        timeDt1 = datetime.strptime(timeStr1, '%H:%M:%S')
        timeDt2 = datetime.strptime(timeStr2, '%H:%M:%S')
        t1 = timedelta(hours=timeDt1.hour, minutes=timeDt1.minute, seconds=timeDt1.second)
        t2 = timedelta(hours=timeDt2.hour, minutes=timeDt2.minute, seconds=timeDt2.second)
        duration = t2 - t1
        return str(duration)
    with open(inputCsv) as csvfile:
        csvdict_count = csv.DictReader(csvfile, dialect='excel')
        for count, row in enumerate(csvdict_count):
            print(row['Title'], count + 1)
        print(count)
        total_tracks = count + 1
    with open(inputCsv) as csvfile:
        #csvreader = csv.reader(csvfile, dialect='excel')
        #for row in csvreader:
        #    print(', '.join(row))
        csvdict = csv.DictReader(csvfile, dialect='excel')
        previousStart = None
        previousEnd = None #end not used not logic
        previousTitle = None
        previousTrackFileName = None
        previousTrackNum = None
        trackNum = 0
        for row2 in csvdict:
            #printTrackInfo(row2['Start'], row2['Title'])
            keepcharacters = (' ','[',']','.','_','!')
            trackNum = trackNum + 1
            trackFileName = "".join(c for c in row2['Title'] if c.isalnum() or c in keepcharacters).strip()
            currentStart = row2['Start']
            currentEnd = row2['End']
            if not currentEnd:
                if previousStart != None:
                    ffmpegSplit(inputFile, previousStart, getTrackTime(previousStart, currentStart), previousTitle, outputDir, previousTrackFileName, str(previousTrackNum).rjust(2, '0'))
                if trackNum == total_tracks:
                    print("Last track")
                    ffmpegSplit(inputFile, currentStart, None, row2['Title'], outputDir, trackFileName, str(trackNum).rjust(2, '0'))
            else:
                ffmpegSplit(inputFile, currentStart, getTrackTime(currentStart, currentEnd), row2['Title'], outputDir, trackFileName, str(trackNum).rjust(2, '0'))
            previousStart = currentStart
            previousEnd = row2['End']
            previousTitle = row2['Title']
            previousTrackFileName = trackFileName
            previousTrackNum = trackNum

class Ergowindow(wx.Frame):
    def __init__(self, parent, title):
        super(Ergowindow, self).__init__(parent, title=title,
            size=(500, 220))
        self.Centre()
        panel = wx.Panel(self)
        self.mediaButtonLabel = wx.TextCtrl(panel, style = wx.TE_READONLY, pos = (150, 15), size = (330, -1))
        self.mediaButtonLabel.SetValue("No media file selected")
        self.mediaButton = wx.Button(panel, label='Open media file', pos=(5, 15))
        self.mediaButton.Bind(wx.EVT_BUTTON, self.on_open_file)
        self.csvButton = wx.Button(panel, label='Open CSV file', pos=(5, 50))
        self.csvButtonLabel = wx.TextCtrl(panel, style = wx.TE_READONLY, pos = (150, 50), size = (330, -1)) 
        self.csvButtonLabel.SetValue("No CSV file selected")
        self.csvButton.Bind(wx.EVT_BUTTON, self.on_open_csvfile)
        self.dirButton = wx.Button(panel, label='Select output dir', pos=(5, 85))
        self.dirButtonLabel = wx.TextCtrl(panel, style = wx.TE_READONLY, pos = (150, 85), size = (330, -1)) 
        self.dirButtonLabel.SetValue("No output directory selected")
        self.dirButton.Bind(wx.EVT_BUTTON, self.on_open_folder)
        self.go_button = wx.Button(panel, label='Go', pos=(400, 155))
        self.go_button.Bind(wx.EVT_BUTTON, self.on_gobutton_click)
        encTypeLabels = ["Copy", "Copy - Audio Only", "Encode", "Encode - Audio Only"]
        self.encTypeChoice = wx.Choice(panel,choices=encTypeLabels,pos=(9150, 9150))
        self.encTypeChoice.Bind(wx.EVT_CHOICE, self.OnChoice)
        self.encTypeLabel = wx.StaticText(panel, label = "Encode or copy type: ", pos = (910, 9150))
        box = wx.BoxSizer(wx.VERTICAL) 
        self.label = wx.StaticText(panel,label = " " ,style = wx.ALIGN_CENTRE, pos=(10, 170)) 
        box.Add(self.label, 0 , wx.EXPAND |wx.ALL, 20) 

    def on_open_folder(self, event):
        title = "Choose a directory:"
        dlg = wx.DirDialog(self, title,
                           style=wx.DD_DEFAULT_STYLE)
        if dlg.ShowModal() == wx.ID_OK:
            self.update_selected_folder(dlg.GetPath())
        dlg.Destroy()
    def on_open_file(self, event):
        title = "Select media file to process"
        dlg = wx.FileDialog(self, title, 
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST, wildcard="Media files (*.mp4;*.mkv;*.avi;*.m4a;*.mp3;*.wav)|*.mp4;*.mkv;*.avi;*.m4a;*.mp3;*.wav")
        if dlg.ShowModal() == wx.ID_OK:
            self.update_selected_mediafile(dlg.GetPath())
        dlg.Destroy()
    def on_open_csvfile(self, event):
        title = "Select CSV file with timestamps"
        dlg = wx.FileDialog(self, title, 
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST, wildcard="CSV files (*.csv)|*.csv")
        if dlg.ShowModal() == wx.ID_OK:
            self.update_selected_csvfile(dlg.GetPath())
        dlg.Destroy()
    def update_selected_folder(self, folder_path):
        print(folder_path)
        global output_dir
        output_dir = folder_path
        self.dirButtonLabel.SetValue(folder_path)
    def update_selected_mediafile(self, file_path):
        print(file_path)
        self.mediaButtonLabel.SetValue(file_path)
        global media_file
        media_file = file_path
    def update_selected_csvfile(self, file_path):
        print(file_path)
        self.csvButtonLabel.SetValue(file_path)
        global csv_file
        csv_file = file_path
    def OnChoice(self,event): 
        self.label.SetLabel("selected "+ self.encTypeChoice.
        GetString( self.encTypeChoice.GetSelection() ) +" from encTypeChoice")
    def on_gobutton_click(self, event):
        global ffmpeg_path
        # Environment checks
        script_path = os.path.dirname(os.path.realpath(__file__))
        if not ffmpeg_path:
            if "darwin" in sys.platform:
                ffmpeg_path = os.path.join(script_path, "ffmpeg")
            else:
                ffmpeg_path = os.path.join(script_path, "ffmpeg.exe") # not handling case for currentdir ffmpeg binary on Linux
        ffmpeg_exists = os.path.isfile(ffmpeg_path)
        print("CSV: ", csv_file)
        print("Media: ", media_file)
        print("Output directory: ", output_dir)
        print("ffmpeg binary: ", ffmpeg_path, ffmpeg_exists)
        if all({media_file, csv_file, output_dir, ffmpeg_exists}):
            do_encode(media_file, csv_file, "copy", output_dir)
        elif not ffmpeg_exists:
            wx.MessageBox('ffmpeg was not found. Please make sure the ffmpeg executable file is in PATH or in the same directory (' + script_path + ') as this app.', 'ffmpeg missing', wx.OK | wx.ICON_EXCLAMATION)
        else:           
            wx.MessageBox('Please make sure to select media file, CSV file and output directory ', 'Parameter missing', wx.OK | wx.ICON_EXCLAMATION)

def main():
    app = wx.App()
    main_window = Ergowindow(None, title='Media File + CSV Timestamp Splitter')
    main_window.Show()
    app.MainLoop()



if __name__ == '__main__':
    main()