#!/usr/bin/env python
# encoding: utf-8
"""
timestampParser.py
 
Matthieu Yiptong - 2019-04-26.
"""
import csv
import subprocess
import os
import sys
from datetime import timedelta, datetime
#inputFile = "ASMR _ WHEN WE ALL FALL ASLEEP, WHERE DO WE GO _ Full Album Billie Eilish Tribute-ZsZA_19xIRI.mp4"
inputFile = sys.argv[1]
inputCsv = sys.argv[2]
encType = sys.argv[3]
if encType == "audioonlycopy":
    encVideo = "remove"
    encAudio = "copy"
elif encType == "enc":
    encVideo = "h264"
    encAudio = "aac"
elif encType == "audioonlyenc":
    encVideo = "remove"
    encAudio = "aac"
else: #encType == "copy":
    encVideo = "copy"
    encAudio = "copy"
def getVideoEncodeFlag():
    if encVideo == "remove":
        return "-vn"
    else:
        return "-c:v"
def printTrackInfo(start, title):
    print("Starting time: " + start)
    print("Segment Title: " + title)
def ffmpegSplit(sourceFile, start, duration, title, outputFile, trackNumStr):
    if duration == None :
        args = ["ffmpeg", "-y", "-ss", start, "-i", sourceFile, getVideoEncodeFlag(), encVideo, "-c:a", encAudio, trackNumStr + " " + outputFile + ".mp4"]
    else:
        args = ["ffmpeg", "-y", "-ss", start, "-i", sourceFile, "-t", duration, getVideoEncodeFlag(), encVideo, "-c:a", encAudio, trackNumStr + " " + outputFile + ".mp4"]
    if encVideo == "remove":
        args.remove("remove")
    print(args)
    FNULL = open(os.devnull, 'w')
    subprocess.call(args) #, stdout=FNULL, stderr=FNULL, shell=False)
def getTrackTime(timeStr1, timeStr2):
    #timeStr1 = "00:10"
    #timeStr2 = "03:01"
    timeDt1 = datetime.strptime(timeStr1, '%H:%M:%S')
    timeDt2 = datetime.strptime(timeStr2, '%H:%M:%S')
    t1 = timedelta(hours=timeDt1.hour, minutes=timeDt1.minute, seconds=timeDt1.second)
    t2 = timedelta(hours=timeDt2.hour, minutes=timeDt2.minute, seconds=timeDt2.second)
    duration = t2 - t1
    return str(duration)
with open(inputCsv) as csvfile:
    csvdict_count = csv.DictReader(csvfile, dialect='excel')
    for count, row in enumerate(csvdict_count):
        print(row['Title'], count + 1)
    print(count)
    total_tracks = count + 1
with open(inputCsv) as csvfile:
    #csvreader = csv.reader(csvfile, dialect='excel')
    #for row in csvreader:
    #    print(', '.join(row))
    csvdict = csv.DictReader(csvfile, dialect='excel')
    previousStart = None
    previousEnd = None #end not used not logic
    previousTitle = None
    previousTrackFileName = None
    previousTrackNum = None
    trackNum = 0
    for row2 in csvdict:
        #printTrackInfo(row2['Start'], row2['Title'])
        keepcharacters = (' ','[',']','.','_','!')
        trackNum = trackNum + 1
        trackFileName = "".join(c for c in row2['Title'] if c.isalnum() or c in keepcharacters).strip()
        currentStart = row2['Start']
        currentEnd = row2['End']
        if not currentEnd:
            if previousStart != None:
                ffmpegSplit(inputFile, previousStart, getTrackTime(previousStart, currentStart), previousTitle, previousTrackFileName, str(previousTrackNum).rjust(2, '0'))
            if trackNum == total_tracks:
                print("Last track")
                ffmpegSplit(inputFile, currentStart, None, row2['Title'], trackFileName, str(trackNum).rjust(2, '0'))
        else:
            ffmpegSplit(inputFile, currentStart, getTrackTime(currentStart, currentEnd), row2['Title'], trackFileName, str(trackNum).rjust(2, '0'))
        previousStart = currentStart
        previousEnd = row2['End']
        previousTitle = row2['Title']
        previousTrackFileName = trackFileName
        previousTrackNum = trackNum


