from datetime import timedelta, datetime
timeStr1 = "00:10"
timeStr2 = "03:01"
timeDt1 = datetime.strptime(timeStr1, '%M:%S')
timeDt2 = datetime.strptime(timeStr2, '%M:%S')
t1 = timedelta(hours=timeDt1.hour, minutes=timeDt1.minute, seconds=timeDt1.second)
t2 = timedelta(hours=timeDt2.hour, minutes=timeDt2.minute, seconds=timeDt2.second)
duration = t2 - t1
print(str(duration))